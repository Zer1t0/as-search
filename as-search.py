#!/usr/bin/env python3

from as_search.__main__ import main

if __name__ == '__main__':
    exit(main())
